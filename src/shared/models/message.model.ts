export class Message {
  severity: number;
  summary: string;
  detail: string;

  static create(error: any) {
    if ([400, 409].includes(error.status))
      return this.warn(error.error);
    else
      return this.error(null);
  }

  static warn(msg: string) {
    return this.generate('warn', 'Atenção', msg);
  }

  static error(msg: string) {
    return this.generate('error', 'Erro', msg);
  }

  private static generate(severity: string, summary: string, detail: string) {
    if ( !detail ) detail = "Falha ao processar dados";
    return {severity, summary, detail };
  }
}
