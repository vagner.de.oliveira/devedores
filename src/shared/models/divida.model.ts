import {Usuario} from "./usuario.model";

export class Divida {
  id: number;
  usuario: Usuario;
  motivo: string;
  data: Date;
  valor: number;
}
