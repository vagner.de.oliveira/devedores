import { Component, OnInit } from '@angular/core';

import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  menus: MenuItem[];

  constructor() { }

  ngOnInit() {
    this.menus = [
      { label: 'Home', icon: 'pi pi-home', routerLink: '/' },
      { label: 'Dívidas', icon: 'pi pi-dollar', routerLink: '/dividas' }
    ];
  }
}
