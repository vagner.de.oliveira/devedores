import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-valid-feedback',
  templateUrl: './valid-feedback.component.html',
  styleUrls: ['./valid-feedback.component.scss']
})
export class ValidFeedbackComponent {
  @Input() show: boolean;
  @Input() element: FormControl;

}
