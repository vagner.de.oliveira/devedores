import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from "./modules/home/home.component";

const routes: Routes = [
  { path: 'dividas', loadChildren: './modules/dividas/dividas.module#DividasModule' },
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
