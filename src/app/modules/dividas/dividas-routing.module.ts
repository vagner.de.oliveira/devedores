import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DividasComponent} from "./dividas.component";
import {DividaComponent} from "./divida/divida.component";

const routes: Routes = [
  { path: '', component: DividasComponent, children: [
    { path: 'nova', component: DividaComponent },
    { path: ':id', component: DividaComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DividasRoutingModule { }
