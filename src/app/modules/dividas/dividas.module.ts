import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {NgxCurrencyModule} from "ngx-currency";

import {ButtonModule} from "primeng/button";
import {ListboxModule} from "primeng/listbox";
import {DropdownModule} from "primeng/dropdown";
import {InputTextModule} from "primeng/inputtext";

import {CalendarModule} from "primeng/calendar";
import {DividasComponent} from "./dividas.component";
import {DividasRoutingModule} from "./dividas-routing.module";
import {DividasService} from "./dividas.service";
import {DividaComponent} from './divida/divida.component';
import {ValidFeedbackComponent} from "../../../shared/components/valid-feedback/valid-feedback.component";

@NgModule({
  declarations: [
    DividasComponent,
    DividaComponent,
    ValidFeedbackComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DividasRoutingModule,
    ButtonModule,
    ListboxModule,
    DropdownModule,
    InputTextModule,
    CalendarModule,
    NgxCurrencyModule
  ],
  providers: [
    DividasService
  ]
})
export class DividasModule { }
