import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {MessageService, SelectItem} from "primeng/api";

import {DividasService} from "./dividas.service";
import {Message} from "../../../shared/models/message.model";

@Component({
  selector: 'app-dividas',
  templateUrl: './dividas.component.html',
  styleUrls: ['./dividas.component.scss']
})
export class DividasComponent implements OnInit {
  dividas: SelectItem[];
  dividaSelecionado: SelectItem;
  manipulando: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: DividasService,
    private message: MessageService) {
  }

  ngOnInit() {
    this.load();
  }

  load() {
    this.service.list().subscribe(
      response => {
        this.dividas = [];
        response.forEach( r => {
          this.dividas.push({label: r.usuario.name, value: r})
        })
      },
      error => {
        this.message.add(Message.error('Falha ao carregar dívidas'));
      }
    );
  }

  editarDivida() {
    this.router.navigate(["dividas", this.dividaSelecionado.value.id]);
  }

  novaDivida() {
    this.router.navigate(["dividas/nova"]);
  }

  onActivate(e) {
    this.manipulando = true;
  }

  onDeactivate(e) {
    this.dividaSelecionado = null;
    this.manipulando = false;
    this.load();
  }

}
