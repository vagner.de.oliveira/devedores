import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {MessageService, SelectItem} from "primeng/api";

import {DividasService} from "../dividas.service";
import {Divida} from "../../../../shared/models/divida.model";
import {Subscription} from "rxjs";
import {Usuario} from "../../../../shared/models/usuario.model";
import {Message} from "../../../../shared/models/message.model";

@Component({
  selector: 'app-divida',
  templateUrl: './divida.component.html'
})
export class DividaComponent implements OnInit, OnDestroy {
  private idDivida: number;
  registerForm: FormGroup;
  isCadastro: boolean = true;
  submitted: boolean = false;
  usuarios: SelectItem[];
  subscription: Subscription;

  constructor(
    private message: MessageService,
    private service: DividasService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      params => {
        this.idDivida = params.id;
        this.isCadastro = !this.idDivida;

        this.registerForm = this.formBuilder.group({
          usuario: this.formBuilder.group({
            id: [{value: null, disabled: !this.isCadastro}, Validators.required]
          }),
          motivo: [ null, [Validators.required, Validators.maxLength(50)]],
          valor: [ null, [Validators.required, Validators.max(10000000.00)]],
          data: [ null, Validators.required]
        });
        this.loadDivida(this.idDivida);
      }
    );

    this.service.listUsuarios().subscribe(
      usuarios => {
        this.usuarios = [{label: 'Selecione...', value: null}];
        usuarios.forEach(u => this.usuarios.push({label: u.name, value: u.id}));
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  loadDivida(id: number) {
    if ( id )
      this.service.find(id).subscribe(divida => this.resetForm(divida) );
    else
      this.resetForm();
  }

  resetForm(divida?: Divida) {
    if ( !divida ) divida = new Divida();
    if ( !divida.usuario ) divida['usuario'] = new Usuario();

    this.registerForm.patchValue({
      usuario: {
        id: divida.usuario.id
      },
      motivo: divida.motivo,
      valor: divida.valor,
      data: divida.data ? new Date(divida.data) : null
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  valid(input: string) {
    const el = input == 'id' ? this.f.usuario['controls'][input] : this.f[input];
    return (this.submitted || el.touched) && el.invalid;
  }

  cancelar() {
    this.router.navigate(["dividas"]);
  }

  excluir() {
    this.service.excluir(this.idDivida).subscribe(
      response => this.cancelar(),
      error => this.message.add(Message.create(error)));  }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.registerForm.controls['usuario']['controls']['id'].enable();

    const divida = Object.assign({}, this.registerForm.value);
    if ( this.isCadastro )
      this.service.save(divida).subscribe(
        response => this.cancelar(),
        error => this.message.add(Message.create(error)));
    else
      this.service.update( this.idDivida, divida).subscribe(
        response => this.cancelar(),
        error => this.message.add(Message.create(error)));

  }
}
