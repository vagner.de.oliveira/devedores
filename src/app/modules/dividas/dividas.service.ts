import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';

import {environment} from 'src/environments/environment';
import {Divida} from "../../../shared/models/divida.model";
import {Usuario} from "../../../shared/models/usuario.model";
import {take} from "rxjs/operators";

@Injectable()
export class DividasService {

  private readonly baseUrl = environment.baseUrl;
  private readonly url = this.baseUrl + "/dividas";
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) {}

  list(): Observable<Divida[]> {
      return this.http.get<Divida[]>(this.url).pipe(take(1));
  }

  listUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.url + "/usuarios").pipe(take(1));
  }

  find(id: number): Observable<Divida> {
    return this.http.get<Divida>(this.url + "/" + id ).pipe(take(1));
  }

  save(divida: Divida): Observable<any> {
    return this.http.post(this.url, divida).pipe(take(1));
  }

  update(idDivida: number, divida: Divida): Observable<any> {
    return this.http.put(this.url + "/" + idDivida, divida).pipe(take(1));
  }

  excluir(id: number): Observable<any> {
    return this.http.delete(this.url + "/" + id).pipe(take(1));
  }
}
